"""
Educational tool to see how the boundary values affect the equilibrium parameters found by VMEC.

Notes:
- currently set for iota
- for periods other than 2, NFP should be changed below
- additional parameters, which are not currently changed by this script, must be changed in the file input.VMEC.header

Assumes:
- VMEC binary in '~/bin/xvmec2000'
- file input.VMEC.header used to construct the input file after boundary parameters are changed

Python Requirements:
- netcdf4 
- numpy
- matplotlib

To Do:
- add drop-down menu to plot other parameters than iota

Questions:
Peter Porazik, pporazik@pppl.gov
"""

from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import Slider, RadioButtons, TextBox, Button
import matplotlib.mlab as mlb
import re
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import os, subprocess
from netCDF4 import Dataset

fW = 18.0
fH = 10.0
fig = plt.figure(figsize=(fW,fH))
fig.subplots_adjust(left=0.1,bottom=0.25)

NFP = 2.0
MZT = 1.0 # max(zt) = 2*pi*MZT

# Initial Boundary Parameters
BDS="""RBC(0,0) = 1.0    ZBS(0,0) = 0.0
  RBC(1,0) = 0.0    ZBS(1,0) = 0.0
  RBC(0,1) = 0.75    ZBS(0,1) = 0.75
  RBC(1,1) = 0.0     ZBS(1,1) = 0.0"""


# Show input file specifications
#fig.text(0.74,0.12,'Modifying Case %s'%(caseName),fontsize=10,color='blue')
#fig.text(0.74,0.15,'RBC(n,m), ZBS(n,m) \n %s \n NFP = %i'%(BDS,NFP),fontsize=8,bbox=dict(facecolor='blue',alpha=0.1))

# M,N ARRAYS
BDS=BDS.split('\n')
BDS2=[BDS[i].split('ZBS') for i in range(0,len(BDS))]
BDSZ = [BDS2[i][1].split('=')[1] for i in range(0,len(BDS2))]
BDSR = [BDS2[i][0].split('=')[1] for i in range(0,len(BDS2))]
ZBS = [float(zval) for zval in BDSZ]
RBC = [float(rval) for rval in BDSR]
n = [int((BDS2[i][1].split(',')[0]).split('(')[1]) for i in range(0,len(BDS2))]
m = [int((BDS2[i][1].split(',')[1]).split(')')[0]) for i in range(0,len(BDS2))]
rlabels=['(%i,%i)'%(m[i],n[i]) for i in range(0,len(n))]

# R,D DATA
Rd = {rlabels[i]:RBC[i] for i in range(0,len(rlabels))}
Zd = {rlabels[i]:ZBS[i] for i in range(0,len(rlabels))}

# M,N RADIO BUTTONS
ax_radio = fig.add_axes([0.001,0.3,0.19,0.4])
ax_radio.axis('off')
radio = RadioButtons(ax_radio,labels=rlabels,active=0)

# R AND Z SLIDERS
sRmx = np.max(np.abs(RBC))
sZmx = np.max(np.abs(RBC))
ax_slider_color = 'lightgoldenrodyellow'
ax_slider_R = fig.add_axes([0.1, 0.05, 0.25, 0.03], axisbg=ax_slider_color)
slider_R = Slider(ax_slider_R, 'Rmn',-sRmx,sRmx,valfmt='%6.4f')
ax_slider_Z = fig.add_axes([0.45, 0.05, 0.25, 0.03], axisbg=ax_slider_color)
slider_Z = Slider(ax_slider_Z, 'Zmn',-sZmx,sZmx,valfmt='%6.4f')

for i in range(0,len(rlabels)):
    radio.circles[i].radius=0.008

def radio_fun(n):
    slider_R.set_val(Rd[n])
    slider_Z.set_val(Zd[n])

# MAIN PLOTTING AXIS
ax_plot = fig.add_axes([0.1,.14,.6,.8],projection='3d')
ax_plot.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax_plot.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax_plot.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
#ax_plot.axis('off')
ax_plot.grid(False)

# NFP TEXT BOX
#ax_txt = fig.add_axes([0.89,0.05,0.03,0.03])
#txt_nfp = TextBox(ax_txt,'NFP',str(NFP))

nth=50
nzt=100
th = np.linspace(0,2.0*np.pi,nth)
zt = np.linspace(0,2.0*np.pi*MZT,nzt)

R = np.zeros([nth,nzt])
Z = np.zeros([nth,nzt])
p = np.zeros([nth,nzt])
for i,rl in enumerate(rlabels):
    for ti in range(0,nth):
        R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
        Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)
        p[ti,:] = zt

im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
im_wth = ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
im_wzt = ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

mxd = np.max([np.max(R),np.max(Z)])
ax_plot.set_xlim([-mxd,mxd])
ax_plot.set_ylim([-mxd,mxd])
ax_plot.set_zlim([-mxd,mxd])

# iota plot
ax_iota = fig.add_axes([0.76,0.24,0.2,0.55])
im_iota = ax_iota.plot(np.zeros(35))
ax_iota.set_xlabel('Flux Surface Index')
ax_iota.set_ylabel('$\\iota$',fontsize=16)
ax_iota.grid('on')

# OUTPUT button
ax_btn = fig.add_axes([0.9,0.05,0.06,0.03])
btn_out = Button(ax_btn,label="RUN VMEC", hovercolor='green')

def buttonWrite(val):

    global im_iota

    outForm = ['RBC(%i,%i) = %8.5e    ZBS(%i,%i) = %8.5e'%(n[i],m[i],Rd[rl],n[i],m[i],Zd[rl]) for i,rl in enumerate(rlabels)]
    np.savetxt('BDS.dat',outForm, fmt='%s')
    os.system('cat input.VMEC.header BDS.dat slash.txt >> input.VMEC')
    process=subprocess.Popen('~/bin/xvmec2000 input.VMEC >& vmec2000.out',shell=True, stdout=subprocess.PIPE)
    process.wait()
    os.system('rm -rf input.VMEC')
        
    vmecData=Dataset('wout_VMEC.nc','r',format='NETCDF4')

    iotas = vmecData.variables['iotas'][1:]
    im_iota.pop(0).remove()
    im_iota = ax_iota.plot(iotas)
    #ax_iota.set_ylim([0,iotas[np.where(iotas==np.max(abs(iotas)))][0]])
    ax_iota.set_ylim([np.min(iotas),np.max(iotas)])
    plt.draw()


def updateR(val):
    Rd[radio.value_selected]=val

    global im_wire, NFP, im_wth, im_wzt
    im_wire.remove()
    im_wth.pop(0).remove()
    im_wzt.pop(0).remove()


    R = np.zeros([nth,nzt])
    Z = np.zeros([nth,nzt])
    for i,rl in enumerate(rlabels):
        for ti in range(0,nth):
            R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
            Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)

    im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
    im_wth=ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
    im_wzt=ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

    mxd = np.max([np.max(R),np.max(Z)])
    ax_plot.set_xlim([-mxd,mxd])
    ax_plot.set_ylim([-mxd,mxd])
    ax_plot.set_zlim([-mxd,mxd])
    plt.draw()

def updateZ(val):
    Zd[radio.value_selected]=val
       
    global im_wire, NFP, im_wth, im_wzt
    im_wire.remove()
    im_wth.pop(0).remove()
    im_wzt.pop(0).remove()

    
    R = np.zeros([nth,nzt])
    Z = np.zeros([nth,nzt])
    for i,rl in enumerate(rlabels):
        for ti in range(0,nth):
            R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
            Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)

    im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
    im_wth=ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
    im_wzt=ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

    mxd = np.max([np.max(R),np.max(Z)])
    ax_plot.set_xlim([-mxd,mxd])
    ax_plot.set_ylim([-mxd,mxd])
    ax_plot.set_zlim([-mxd,mxd])
    plt.draw()


btn_out.on_clicked(buttonWrite)
radio.on_clicked(radio_fun)    
slider_R.on_changed(updateR)
slider_Z.on_changed(updateZ)

fig.set_facecolor('white')
fig.show()
