# README #
![CNT](images/bdvis_CNT.png)
![InteractiveExample](images/interact.png)

* __bdvis.py__ is a simple browser of VMEC boundary shape data
* __interact.py__ is a tool to see how the boundary shape changes equilibrium parameters


### Example 1:
1) Change the variable __caseName__ to select a case from the those given at the top of the source file, or write your own case.

2) In ipython, do

   %run -i bdvis3.py
   
3) Figure will be displayed

   - The left side of the Figure will show radio buttons corresponding to the available modes for the chosen case. Note that the mode numbers are switched with respect to __RBC__ and __ZBS__, i.e. they correspond to (m,n).

   - A particular mode can be chosen by clicking the corresponding radio button, and Fourier amplitudes modified by moving the sliders __Rmn__, and __Zmn__.

   - The right side of the Figure contains the data of the case being modified.

4) The results may be saved by clicking the __Write__ button, at which point, __BDS.dat__ is written.

5) The output format may be pasted into a VMEC input file.

### Example 2:
1) In ipython, do

   %run -i interact.py

2) Figure will be displayed

   - Vary the sliders, and the radio buttons to modify the __Rmn__ and __Zmn__ harmonics
   - When satisfied, click __RUN VMEC__ button, at which point VMEC will be run, and the iota profile displayed

### Requirements:
- The interface was created with the version 2.1.0 of matplotlib
- netCDF4 python package, and VMEC binary is required for __interact.py__
