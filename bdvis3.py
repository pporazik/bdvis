"""
Basic browser of boundary shape data.

Example:
1) Change the variable caseName to select a case from the ones below, or write your own.
2) In ipython, do
   %run -i bdvis3.py
3) The results may be saved by clicking the Write button, at which point, BDS.dat is written.
4) The output format may be pasted into a VMEC input file.

Requirements:
- The interface was created with the version 2.1.0 of matplotlib

Questions:
Peter Porazik, pporazik@pppl.gov
"""


from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import Slider, RadioButtons, TextBox, Button
import matplotlib.mlab as mlb
import re
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d.art3d import Line3DCollection

fW = 18.0
fH = 10.0
fig = plt.figure(figsize=(fW,fH))
fig.subplots_adjust(left=0.1,bottom=0.25)

caseName='CNT'

if (caseName=='EXP'):
    NFP = 2
    MZT = 1.0 # max(zt) = 2*pi*MZT
    BDS = """RBC( 00,00) = 1.00  ZBS( 00,00) = 0.00
      RBC( 01,00) = 0.10  ZBS( 01,00) = 0.1
      RBC( 00,01) = 0.10  ZBS( 00,01) = 0.10
      RBC( 01,01) = 0.01  ZBS( 01,01) =-0.01"""          


if (caseName=='TREFOIL'):
    NFP = 1.5 # 1.5 for trefoil
    MZT = 2.0 # max(zt) = 2*pi*MZT
    BDS="""RBC(0,0) = 5.15556e-01    ZBS(0,0) = 0.00000e+00
           RBC(0,1) = 4.00000e-02    ZBS(0,1) = -1.20000e-01
           RBC(1,0) = 3.51111e-01    ZBS(1,0) = 5.55556e-01
           RBC(1,1) = -4.44444e-03    ZBS(1,1) = 4.44444e-03
           RBC(2,0) = -2.22222e-02    ZBS(2,0) = 0.00000e+00
           RBC(2,1) = 0.00000e+00    ZBS(2,1) = 0.00000e+00
           RBC(2,2) = 0.00000e+00    ZBS(2,2) = 0.00000e+00
           RBC(0,2) = 0.00000e+00    ZBS(0,2) = 0.00000e+00
           RBC(1,2) = 0.00000e+00    ZBS(1,2) = 0.00000e+00
           RBC(3,0) = 0.00000e+00    ZBS(3,0) = 0.00000e+00
           RBC(3,1) = 0.00000e+00    ZBS(3,1) = 0.00000e+00"""
    

if (caseName=='CNT'):
    NFP = 2
    MZT = 1.0 # max(zt) = 2*pi*MZT
    BDS = """RBC(00,00) = 2.428333157538E-01  ZBS(00,00) = 0.000000000000E+00
    RBC(01,00) = 3.756465319945E-02  ZBS(01,00) = 5.523399554497E-02
    RBC(02,00) = -1.151519581840E-04  ZBS(02,00) = 5.246401210113E-03
    RBC(03,00) = -1.033450906995E-04  ZBS(03,00) = -2.531785846636E-03
    RBC(04,00) = -4.858804586662E-05  ZBS(04,00) = -2.029329736122E-04
    RBC(04,01) = -8.268577908267E-05  ZBS(04,01) = -2.671400751324E-04
    RBC(03,01) = -6.350902232448E-04  ZBS(03,01) = -1.771683465790E-03
    RBC(02,01) = -1.216455973967E-03  ZBS(02,01) = 1.045667420499E-03
    RBC(01,01) = -1.317427349298E-03  ZBS(01,01) = 1.595492618010E-03
    RBC(00,01) = 6.906124542218E-02  ZBS(00,01) = 1.436396127266E-01
    RBC(-01,01) = 3.248239824815E-02  ZBS(-01,01) = -4.817408998265E-02
    RBC(-02,01) = 4.013483875085E-03  ZBS(-02,01) = -8.247803678942E-03
    RBC(-03,01) = 1.110557552304E-03  ZBS(-03,01) = 8.074284382263E-04
    RBC(-04,01) = -1.318004269713E-06  ZBS(-04,01) = 1.537267084618E-04
    RBC(04,02) = 1.068402414903E-04  ZBS(04,02) = -5.758468369881E-04
    RBC(03,02) = -8.692591757528E-04  ZBS(03,02) = -9.242264239023E-04
    RBC(02,02) = 8.126164306705E-04  ZBS(02,02) = 2.756920306001E-03
    RBC(01,02) = -1.328981225494E-03  ZBS(01,02) = 1.378411664019E-03
    RBC(00,02) = 4.574568140657E-03  ZBS(00,02) = -1.066845530441E-03
    RBC(-01,02) = -2.314751928687E-02  ZBS(-01,02) = -2.487094874671E-03
    RBC(-02,02) = -3.552093950016E-03  ZBS(-02,02) = -4.115821311476E-04
    RBC(-03,02) = -3.918675613617E-04  ZBS(-03,02) = 1.066383187060E-04
    RBC(-04,02) = -1.057261676275E-04  ZBS(-04,02) = 3.667773368475E-04
    RBC(04,03) = -1.407560124997E-04  ZBS(04,03) = -4.296405712437E-04
    RBC(03,03) = -1.111675925833E-03  ZBS(03,03) = 1.441790424783E-05
    RBC(02,03) = 7.184149455141E-05  ZBS(02,03) = 2.113285249409E-03
    RBC(01,03) = 2.041431437127E-04  ZBS(01,03) = 1.770341892171E-03
    RBC(00,03) = -4.489010043330E-04  ZBS(00,03) = 9.528665644086E-06
    RBC(-01,03) = 4.900684970773E-03  ZBS(-01,03) = -8.213664165489E-04
    RBC(-02,03) = 1.838446914436E-03  ZBS(-02,03) = 5.701600546997E-05
    RBC(-03,03) = 1.002234885099E-04  ZBS(-03,03) = 9.870767104417E-05
    RBC(-04,03) = 1.109675065306E-04  ZBS(-04,03) = 2.957585157032E-05
    RBC(04,04) = 5.019010143706E-05  ZBS(04,04) = -2.576524535977E-04
    RBC(03,04) = 1.579976335447E-04  ZBS(03,04) = 7.797258384169E-04
    RBC(02,04) = 3.615563986628E-04  ZBS(02,04) = 3.443004754933E-04
    RBC(01,04) = 1.115372604854E-03  ZBS(01,04) = 9.442196834600E-04
    RBC(00,04) = 2.445920516078E-04  ZBS(00,04) = -9.467574409014E-06
    RBC(-01,04) = 5.060996690651E-04  ZBS(-01,04) = 9.465090765813E-04
    RBC(-02,04) = -7.497843742466E-04  ZBS(-02,04) = -1.377848693924E-03
    RBC(-03,04) = -6.417400481953E-04  ZBS(-03,04) = 3.314010469624E-04
    RBC(-04,04) = -1.015768899169E-05  ZBS(-04,04) = 2.490066138588E-04
    RBC(04,05) = -4.684346623968E-05  ZBS(04,05) = -1.830053484465E-04
    RBC(03,05) = 6.673715509789E-04  ZBS(03,05) = 1.242786665654E-03
    RBC(02,05) = 6.305260998600E-04  ZBS(02,05) = 7.140133793166E-04
    RBC(01,05) = 9.969028438339E-04  ZBS(01,05) = 2.293919724196E-04
    RBC(00,05) = 3.353507046097E-04  ZBS(00,05) = 1.668934136130E-04
    RBC(-01,05) = 2.738398509830E-04  ZBS(-01,05) = 2.142490617361E-04
    RBC(-02,05) = -6.990119564854E-04  ZBS(-02,05) = -2.721940965855E-04
    RBC(-03,05) = 1.233723066352E-04  ZBS(-03,05) = 6.419159768191E-04
    RBC(-04,05) = 1.070086519835E-04  ZBS(-04,05) = 1.246634766788E-04
    RBC(04,06) = -6.121192776552E-05  ZBS(04,06) = -2.141209743403E-04
    RBC(03,06) = 1.476949792686E-04  ZBS(03,06) = 6.705084044340E-05
    RBC(02,06) = -4.362442663171E-05  ZBS(02,06) = 1.872039640496E-04
    RBC(01,06) = -3.963444669860E-04  ZBS(01,06) = -5.699249176273E-04
    RBC(00,06) = 5.395009165930E-05  ZBS(00,06) = 4.187387907868E-04
    RBC(-01,06) = -2.109461765496E-04  ZBS(-01,06) = 4.691360536877E-04
    RBC(-02,06) = 1.400173218112E-04  ZBS(-02,06) = -6.767803801640E-07
    RBC(-03,06) = 4.332365716965E-04  ZBS(-03,06) = 2.487272657988E-04
    RBC(-04,06) = 8.880337843244E-05  ZBS(-04,06) = -9.522651097757E-05"""


if (caseName=='LPKU'):
    NFP = 3
    ZTF = 1.0 # max(zt) = 2*pi*MZT
    BDS = """RBC(0,0) =   9.12881000000000E+00     ZBS(0,0) =   0.00000000000000E+00
  RBC(1,0) =   1.19508640282799E-01     ZBS(1,0) =  -1.19508640282799E-01
  RBC(2,0) =  -5.35372655894273E-02     ZBS(2,0) =   5.35372655894273E-02
  RBC(-1,1) =  2.76193562308813E-01     ZBS(-1,1) =  7.31279263335229E-02
  RBC(0,1) =   1.21118000000000E+00     ZBS(0,1) =   2.31482000000000E+00
  RBC(1,1) =  -6.21782068262065E-01     ZBS(1,1) =   6.21782068262065E-01
  RBC(2,1) =  -2.19120624596598E-02     ZBS(2,1) =   2.19120624596598E-02
  RBC(-1,2) = -1.09847160363532E-02     ZBS(-1,2) =  1.09847160363532E-02
  RBC(0,2) =   4.42510000000000E-01     ZBS(0,2) =  -2.29200000000000E-02
  RBC(1,2) =   3.09399696995964E-01     ZBS(1,2) =   5.04002614217115E-02
  RBC(2,2) =   9.88783457596413E-02     ZBS(2,2) =  -9.88783457596413E-02
  RBC(0,3) =  -3.53000000000000E-03     ZBS(0,3) =   3.53000000000000E-03
  RBC(3,3) =  -2.99700000000000E-02     ZBS(3,3) =   2.99700000000000E-02"""



# Show input file specifications
fig.text(0.73,0.95,'Modifying Case %s'%(caseName),fontsize=10,color='red')
fig.text(0.74,0.90,'NFP = %i'%(NFP),fontsize=8)
fig.text(0.74,0.15,'RBC(n,m), ZBS(n,m) \n %s'%(BDS),fontsize=8)

# M,N ARRAYS
BDS=BDS.split('\n')
BDS2=[BDS[i].split('ZBS') for i in range(0,len(BDS))]
BDSZ = [BDS2[i][1].split('=')[1] for i in range(0,len(BDS2))]
BDSR = [BDS2[i][0].split('=')[1] for i in range(0,len(BDS2))]
ZBS = [float(zval) for zval in BDSZ]
RBC = [float(rval) for rval in BDSR]
n = [int((BDS2[i][1].split(',')[0]).split('(')[1]) for i in range(0,len(BDS2))]
m = [int((BDS2[i][1].split(',')[1]).split(')')[0]) for i in range(0,len(BDS2))]
rlabels=['(%i,%i)'%(m[i],n[i]) for i in range(0,len(n))]

# R,D DATA
Rd = {rlabels[i]:RBC[i] for i in range(0,len(rlabels))}
Zd = {rlabels[i]:ZBS[i] for i in range(0,len(rlabels))}

# M,N RADIO BUTTONS
ax_radio = fig.add_axes([0.001,0.05,0.19,0.95])
ax_radio.axis('off')
radio = RadioButtons(ax_radio,labels=rlabels,active=0)

# R AND Z SLIDERS
sRmx = np.max(np.abs(RBC))
sZmx = np.max(np.abs(RBC))
ax_slider_color = 'lightgoldenrodyellow'
ax_slider_R = fig.add_axes([0.2, 0.05, 0.25, 0.03], axisbg=ax_slider_color)
slider_R = Slider(ax_slider_R, 'Rmn',-sRmx,sRmx,valfmt='%6.4f')
ax_slider_Z = fig.add_axes([0.55, 0.05, 0.25, 0.03], axisbg=ax_slider_color)
slider_Z = Slider(ax_slider_Z, 'Zmn',-sZmx,sZmx,valfmt='%6.4f')

for i in range(0,len(rlabels)):
    radio.circles[i].radius=0.008

def radio_fun(n):
    slider_R.set_val(Rd[n])
    slider_Z.set_val(Zd[n])

# MAIN PLOTTING AXIS
ax_plot = fig.add_axes([0.17,.14,.6,.8],projection='3d')
ax_plot.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax_plot.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax_plot.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
#ax_plot.axis('off')
ax_plot.grid(False)

# NFP TEXT BOX
ax_txt = fig.add_axes([0.89,0.05,0.03,0.03])
txt_nfp = TextBox(ax_txt,'NFP',str(NFP))

nth=50
nzt=100
th = np.linspace(0,2.0*np.pi,nth)
zt = np.linspace(0,2.0*np.pi*MZT,nzt)

R = np.zeros([nth,nzt])
Z = np.zeros([nth,nzt])
p = np.zeros([nth,nzt])
for i,rl in enumerate(rlabels):
    for ti in range(0,nth):
        R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
        Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)
        p[ti,:] = zt

im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
im_wth = ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
im_wzt = ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

mxd = np.max([np.max(R),np.max(Z)])
ax_plot.set_xlim([-mxd,mxd])
ax_plot.set_ylim([-mxd,mxd])
ax_plot.set_zlim([-mxd,mxd])

# OUTPUT button
ax_btn = fig.add_axes([0.95,0.05,0.03,0.03])
btn_out = Button(ax_btn,label="Write", hovercolor='green')

def buttonWrite(val):
    outForm = ['RBC(%i,%i) = %8.5e    ZBS(%i,%i) = %8.5e'%(n[i],m[i],Rd[rl],n[i],m[i],Zd[rl]) for i,rl in enumerate(rlabels)]
    np.savetxt('BDS.dat',outForm, fmt='%s')

def updateNFP(val):
    global NFP
    NFP = float(val)
    
    global im_wire, im_wth, im_wzt
    im_wire.remove()
    im_wth.pop(0).remove()
    im_wzt.pop(0).remove()
    
    R = np.zeros([nth,nzt])
    Z = np.zeros([nth,nzt])
    for i,rl in enumerate(rlabels):
        for ti in range(0,nth):
            R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
            Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)


    im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
    im_wth=ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
    im_wzt=ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

    mxd = np.max([np.max(R),np.max(Z)])
    ax_plot.set_xlim([-mxd,mxd])
    ax_plot.set_ylim([-mxd,mxd])
    ax_plot.set_zlim([-mxd,mxd])
    plt.draw()


def updateR(val):
    Rd[radio.value_selected]=val

    global im_wire, NFP, im_wth, im_wzt
    im_wire.remove()
    im_wth.pop(0).remove()
    im_wzt.pop(0).remove()


    R = np.zeros([nth,nzt])
    Z = np.zeros([nth,nzt])
    for i,rl in enumerate(rlabels):
        for ti in range(0,nth):
            R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
            Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)

    im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
    im_wth=ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
    im_wzt=ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

    mxd = np.max([np.max(R),np.max(Z)])
    ax_plot.set_xlim([-mxd,mxd])
    ax_plot.set_ylim([-mxd,mxd])
    ax_plot.set_zlim([-mxd,mxd])
    plt.draw()

def updateZ(val):
    Zd[radio.value_selected]=val
       
    global im_wire, NFP, im_wth, im_wzt
    im_wire.remove()
    im_wth.pop(0).remove()
    im_wzt.pop(0).remove()

    
    R = np.zeros([nth,nzt])
    Z = np.zeros([nth,nzt])
    for i,rl in enumerate(rlabels):
        for ti in range(0,nth):
            R[ti,:] = R[ti,:] + Rd[rl]*np.cos(m[i]*th[ti] - NFP*n[i]*zt)
            Z[ti,:] = Z[ti,:] + Zd[rl]*np.sin(m[i]*th[ti] - NFP*n[i]*zt)

    im_wire=ax_plot.plot_wireframe(R*np.cos(p),R*np.sin(p),Z,linewidth=0.2)
    im_wth=ax_plot.plot(R[:,0]*np.cos(p[:,0]),R[:,0]*np.sin(p[:,0]),Z[:,0],color='red',linewidth=0.4)
    im_wzt=ax_plot.plot(R[nth/4,:]*np.cos(p[nth/4,:]),R[nth/4,:]*np.sin(p[nth/4,:]),Z[nth/4,:],color='red',linewidth=0.4)

    mxd = np.max([np.max(R),np.max(Z)])
    ax_plot.set_xlim([-mxd,mxd])
    ax_plot.set_ylim([-mxd,mxd])
    ax_plot.set_zlim([-mxd,mxd])
    plt.draw()


btn_out.on_clicked(buttonWrite)
radio.on_clicked(radio_fun)    
txt_nfp.on_submit(updateNFP)
slider_R.on_changed(updateR)
slider_Z.on_changed(updateZ)

fig.set_facecolor('white')
fig.show()