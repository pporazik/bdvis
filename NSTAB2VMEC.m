%% Convert the Fourier Coefficients from NSTAB convenction to VMEC convention
% Tested against the MHH2 coeffiecients given in Table II of Garabedian PoP,
% 1997.
% 
% The NSTAB coefficients are stored in Dmn array. The m's and n's are
% stored in the mn array below.
% 
%        __                                                      
% r  =  \              Delta          cos [ (m + 1)u  -  n v ]  +
%       /__ n,m > =  0       - m, - n                            
%                                                                
%               __                                               
%              \         Delta    cos[(m - 1)u - n v]            
%              /__ n,m>0      m,n                                
%
% 
% 
% 
% 
%        __                                                      
% z  =  \              Delta          sin [ (m + 1)u  -  n v ]  -
%       /__ n,m > =  0       - m, - n                            
%                                                                
%               __                                               
%              \         Delta    sin[(m - 1)u + n v]            
%              /__ n,m>0      m,n                                
% 
% 
% Which can be put into form
%
%
%
%        __                                                                     
% r  =  \           (Delta               +  Delta       ) cos [ M u  -  n v ]  +
%       /__ n,M > 0        - M + 1, - n          M + 1,n                        
%                                                                               
%                              __                                               
%                             \     Delta    cos[n v]                           
%                             /__ n      1,n                                    
% 
% 
%
%        __                                                                     
% z  =  \           (Delta               -  Delta       ) sin [ M u  -  n v ]  -
%       /__ n,M > 0        - M + 1, - n          M + 1,n                        
%                                                                               
%                              __                                               
%                             \     Delta    sin[n v]                           
%                             /__ n      1,n                                    
% 
%
% where M is the poloidal mode number used in VMEC, n is the toroidal mode
% number, u is the poloidal angle and v is the toroidal angle.
%
% The range of M is chosen from 1 to |min(m)|+1 if |min(m)| > max(m);
% or from 1 to max(m) - 1 otherwise.
%
%
% - Peter Porazik (pporazik@pppl.gov)
%
%% MHH2 by Garabedian
clear all;
Dmn = [0; 
  0.27;
  0.195;
  -0.060;
  1.5;
  -0.020;
  0.015;
  0.060;
  5.25;
  0.525;
  0.060;
  0.0;
  0.0;
  0.060;
 -0.705;
 -0.150;
  0.0;
  0.030;
 -0.070;
  0.105;
  0.030;
  0.022;
  0.00
  -0.022;
  -0.022;
  0.0;
  0.0;
  0.0;
  0.015;
  0.0;
  0.0; ];
  
mn = [-2 -1;
  -1 -1;
  -1 0;
  -1 1;
  0 0;
  0 1;
  0 2;
  1 -1;
  1 0;
  1 1;
  1 2;
  1 3;
  2 -1;
  2 0;
  2 1;
  2 2;
  2 3;
  3 0;
  3 1;
  3 2;
  3 3;
  4 0;
  4 1;
  4 2;
  4 3;
  4 4;
  5 1;
  5 2;
  5 3;
  5 4;
  6 3];
  
N = size(mn,1);
L = 2;
v = linspace(0,2*pi,50);
u = linspace(0,2*pi,100);
r = zeros(length(v),length(u));
z = zeros(length(v),length(u));
for i  = 1:1:N
    for j = 1:1:length(v)
        r(j,:) = r(j,:)+Dmn(i)*cos(L*mn(i,2)*v(j)-(1-mn(i,1))*u);
        z(j,:) = z(j,:)+Dmn(i)*sin(L*mn(i,2)*v(j)-(1-mn(i,1))*u);
    end
end;

% transform for 3D plotting
X = zeros(length(v),length(u));
Y = zeros(length(v),length(u));
for i = 1:1:length(u)
    X(:,i) = r(:,i).*cos(v');
    Y(:,i) = r(:,i).*sin(v');
end
figure;surf(X,Y,z);axis equal;

%% TO VMEC
%for i = 1:1:N
%  so = sprintf('RBC( %i, %i ) = %f ZBS( %i, %i ) = %f',mn(i,2),1-mn(i,1),Dmn(i),mn(i,2),1-mn(i,1),Dmn(i));
%  disp(so)
%end;
%
% The simple conversion above will not work for VMEC because VMEC uses only
% m>=0 fourier harmonics, although n can be positive or negative.

if (abs(min(mn(:,1)))>max(mn(:,1)))
    maxM = abs(min(mn(:,1)))+1;
else
    maxM = max(mn(:,1)) - 1;
end

lN = max(abs(mn(:,2)))*2+1;
RBC = zeros(maxM+1,lN);
ZBS = zeros(maxM+1,lN);
for i = 1:1:maxM
    findiNegm = find(mn(:,1)==-i+1);
    findiPosm = find(mn(:,1)==i+1);
    for j = 1:1:lN
        Dnegm = 0;
        Dposm = 0;
        N = -(lN-1)/2 + (j-1);
        findiNegmn = find(mn(findiNegm,2)==-N);
        findiPosmn = find(mn(findiPosm,2)==N);
        if (length(findiNegmn)==1)
            Dnegm = Dmn(findiNegm(findiNegmn));
        end
        if (length(findiPosmn)==1)
            Dposm = Dmn(findiPosm(findiPosmn));
        end
        if (length(findiNegmn)>1 || length(findiPosmn) > 1)
            disp('Problem occured at i = %i j = %i',i,j);
        end
        RBC(i+1,j) = Dnegm + Dposm;
        ZBS(i+1,j) = Dnegm - Dposm;
    end
end

% M=0 component
findiPosm = find(mn(:,1)==1);
for j = 1:1:lN    
    Dposm = 0;
    N = -(lN-1)/2 + (j-1);
    findiPosmn = find(mn(findiPosm,2)==N);
    if (length(findiPosmn)==1)
        findiPosm(findiPosmn)
        Dposm = Dmn(findiPosm(findiPosmn));
    end
    if (length(findiNegmn)>1 || length(findiPosmn) > 1)
        disp('Problem occured at i = %i j = %i',i,j);
    end
    RBC(1,j) =   Dposm;
    ZBS(1,j) = - Dposm;
end

% Plot VMEC coords
zt = linspace(0,2*pi,50);
th = linspace(0,2*pi,100);
R = zeros(length(zt),length(th));
Z = zeros(length(zt),length(th));
for mi = 1:1:maxM+1
    M = mi-1;
    for ni = 1:1:lN
        N = -(lN-1)/2 + (ni-1);
        for j = 1:1:length(zt)        
            R(j,:) = R(j,:)+RBC(mi,ni)*cos(M*th - L*N*zt(j));
            Z(j,:) = Z(j,:)+ZBS(mi,ni)*sin(M*th - L*N*zt(j));            
        end
    end
end

%transform for plotting
X = zeros(length(zt),length(th));
Y = zeros(length(zt),length(th));
for i = 1:1:length(th)
    X(:,i) = R(:,i).*cos(zt');
    Y(:,i) = R(:,i).*sin(zt');
end
figure;surf(X,Y,Z);axis equal;


% print out RBCs and ZBSs
for mi = 1:1:maxM+1
    M = mi-1;
    for ni = 1:1:lN             
        N = -(lN-1)/2 + (ni-1);       
        so = sprintf('RBC( %i, %i ) = %f ZBS( %i, %i ) = %f',N,M,RBC(mi,ni),N,M,ZBS(mi,ni));
        disp(so)
    end
end